package main

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

func handleGraphData(connectionID string, graphData []GraphData, db *gorm.DB) bool {

	query := "INSERT INTO graph_entries (connection, value, measured_at, created_at) VALUES "

	for idx, entry := range graphData {
		if idx > 0 {
			query += ","
		}

		query += fmt.Sprintf("('%s', %f, '%s', NOW())\n", connectionID, entry.Value, entry.Timestamp)
	}

	query += "ON CONFLICT (connection, measured_at) DO NOTHING"

	err := db.Exec(query).Error
	if err != nil {
		log.WithField("error", err).Warning("Failed inserting data")
		return false
	}

	return true
}
