package main

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	log "github.com/sirupsen/logrus"

	"time"
)

var authToken = ""

type LibreLinkUpLoginInput struct {
	Email    string
	Password string
}

type LibreLinkUpLoginResponse struct {
	Status int `json:"status"`
	Data   struct {
		Step struct {
			Type          string `json:"type"`
			ComponentName string `json:"componentName"`
			Props         struct {
				Reaccept bool   `json:"reaccept"`
				TitleKey string `json:"titleKey"`
				Type     string `json:"type"`
			} `json:"props"`
		} `json:"step"`
		User struct {
			AccountType string `json:"accountType"`
			Country     string `json:"country"`
			UILanguage  string `json:"uiLanguage"`
		} `json:"user"`
		AuthTicket struct {
			Token    string `json:"token"`
			Expires  int    `json:"expires"`
			Duration int    `json:"duration"`
		} `json:"authTicket"`
	} `json:"data"`
}

type LibreLinkLogbookRespoonse struct {
	Status int                     `json:"status"`
	Data   []LibreLinkLogbookEntry `json:"data"`
	Ticket struct {
		Token    string `json:"token"`
		Expires  int    `json:"expires"`
		Duration int64  `json:"duration"`
	} `json:"ticket"`
}

type LibreLinkLogbookEntry struct {
	FactoryTimestamp string      `json:"FactoryTimestamp"`
	Timestamp        string      `json:"Timestamp"`
	Type             int         `json:"type"`
	ValueInMgPerDl   int         `json:"ValueInMgPerDl"`
	TrendArrow       int         `json:"TrendArrow"`
	TrendMessage     interface{} `json:"TrendMessage"`
	MeasurementColor int         `json:"MeasurementColor"`
	GlucoseUnits     int         `json:"GlucoseUnits"`
	Value            float64     `json:"Value"`
	IsHigh           bool        `json:"isHigh"`
	IsLow            bool        `json:"isLow"`
}

type LibreLinkGraphResponse struct {
	Status int `json:"status"`
	Data   struct {
		Connection struct {
			ID         string `json:"id"`
			PatientID  string `json:"patientId"`
			Country    string `json:"country"`
			Status     int    `json:"status"`
			FirstName  string `json:"firstName"`
			LastName   string `json:"lastName"`
			TargetLow  int    `json:"targetLow"`
			TargetHigh int    `json:"targetHigh"`
			Uom        int    `json:"uom"`
			Sensor     struct {
				DeviceID string `json:"deviceId"`
				Sn       string `json:"sn"`
				A        int    `json:"a"`
				W        int    `json:"w"`
				Pt       int    `json:"pt"`
			} `json:"sensor"`
			AlarmRules struct {
				H struct {
					On   bool    `json:"on"`
					Th   int     `json:"th"`
					Thmm float64 `json:"thmm"`
					D    int     `json:"d"`
					F    float64 `json:"f"`
				} `json:"h"`
				F struct {
					Th   int     `json:"th"`
					Thmm int     `json:"thmm"`
					D    int     `json:"d"`
					Tl   int     `json:"tl"`
					Tlmm float64 `json:"tlmm"`
				} `json:"f"`
				L struct {
					On   bool    `json:"on"`
					Th   int     `json:"th"`
					Thmm float64 `json:"thmm"`
					D    int     `json:"d"`
					Tl   int     `json:"tl"`
					Tlmm float64 `json:"tlmm"`
				} `json:"l"`
				Nd struct {
					I int `json:"i"`
					R int `json:"r"`
					L int `json:"l"`
				} `json:"nd"`
				P   int `json:"p"`
				R   int `json:"r"`
				Std struct {
				} `json:"std"`
			} `json:"alarmRules"`
			GlucoseMeasurement struct {
				FactoryTimestamp string      `json:"FactoryTimestamp"`
				Timestamp        string      `json:"Timestamp"`
				Type             int         `json:"type"`
				ValueInMgPerDl   int         `json:"ValueInMgPerDl"`
				TrendArrow       int         `json:"TrendArrow"`
				TrendMessage     interface{} `json:"TrendMessage"`
				MeasurementColor int         `json:"MeasurementColor"`
				GlucoseUnits     int         `json:"GlucoseUnits"`
				Value            float64     `json:"Value"`
				IsHigh           bool        `json:"isHigh"`
				IsLow            bool        `json:"isLow"`
			} `json:"glucoseMeasurement"`
			GlucoseItem struct {
				FactoryTimestamp string      `json:"FactoryTimestamp"`
				Timestamp        string      `json:"Timestamp"`
				Type             int         `json:"type"`
				ValueInMgPerDl   int         `json:"ValueInMgPerDl"`
				TrendArrow       int         `json:"TrendArrow"`
				TrendMessage     interface{} `json:"TrendMessage"`
				MeasurementColor int         `json:"MeasurementColor"`
				GlucoseUnits     int         `json:"GlucoseUnits"`
				Value            float64     `json:"Value"`
				IsHigh           bool        `json:"isHigh"`
				IsLow            bool        `json:"isLow"`
			} `json:"glucoseItem"`
			GlucoseAlarm  interface{} `json:"glucoseAlarm"`
			PatientDevice struct {
				Did                 string `json:"did"`
				Dtid                int    `json:"dtid"`
				V                   string `json:"v"`
				Ll                  int    `json:"ll"`
				Hl                  int    `json:"hl"`
				FixedLowAlarmValues struct {
					Mgdl  int     `json:"mgdl"`
					Mmoll float64 `json:"mmoll"`
				} `json:"fixedLowAlarmValues"`
				Alarms            bool `json:"alarms"`
				FixedLowThreshold int  `json:"fixedLowThreshold"`
			} `json:"patientDevice"`
			Created int `json:"created"`
		} `json:"connection"`
		ActiveSensors []struct {
			Sensor struct {
				DeviceID string `json:"deviceId"`
				Sn       string `json:"sn"`
				A        int    `json:"a"`
				W        int    `json:"w"`
				Pt       int    `json:"pt"`
			} `json:"sensor"`
			Device struct {
				Did                 string `json:"did"`
				Dtid                int    `json:"dtid"`
				V                   string `json:"v"`
				Ll                  int    `json:"ll"`
				Hl                  int    `json:"hl"`
				FixedLowAlarmValues struct {
					Mgdl  int     `json:"mgdl"`
					Mmoll float64 `json:"mmoll"`
				} `json:"fixedLowAlarmValues"`
				Alarms            bool `json:"alarms"`
				FixedLowThreshold int  `json:"fixedLowThreshold"`
			} `json:"device"`
		} `json:"activeSensors"`
		GraphData []GraphData `json:"graphData"`
	} `json:"data"`
	Ticket struct {
		Token    string `json:"token"`
		Expires  int    `json:"expires"`
		Duration int64  `json:"duration"`
	} `json:"ticket"`
}

type GraphData struct {
	FactoryTimestamp string  `json:"FactoryTimestamp"`
	Timestamp        string  `json:"Timestamp"`
	Type             int     `json:"type"`
	ValueInMgPerDl   int     `json:"ValueInMgPerDl"`
	MeasurementColor int     `json:"MeasurementColor"`
	GlucoseUnits     int     `json:"GlucoseUnits"`
	Value            float64 `json:"Value"`
	IsHigh           bool    `json:"isHigh"`
	IsLow            bool    `json:"isLow"`
}

func ensureAuth() {

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{
		Timeout:   time.Second * 30,
		Transport: tr,
	}

	loginBody := LibreLinkUpLoginInput{
		Email:    os.Getenv("LIBRE_USERNAME"),
		Password: os.Getenv("LIBRE_PASSWORD"),
	}

	jsonLoginBody, _ := json.Marshal(loginBody)
	req, err := http.NewRequest("POST", "https://api-eu.libreview.io/llu/auth/login", bytes.NewBuffer(jsonLoginBody))
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("version", "4.1.1")
	req.Header.Set("product", "llu.ios")

	response, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	responseData := LibreLinkUpLoginResponse{}
	responseBody, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal(responseBody, &responseData)
	log.Info("Login response received")

	if responseData.Status == 0 {
		authToken = responseData.Data.AuthTicket.Token
	}
}

func getGraphForConnection(connectionID string) ([]GraphData, error) {

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{
		Timeout:   time.Second * 30,
		Transport: tr,
	}

	url := fmt.Sprintf("https://api.libreview.io/llu/connections/%s/graph", connectionID)
	log.WithField("url", url).Info("Sending graph request")

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", authToken))
	req.Header.Set("version", "4.1.1")
	req.Header.Set("product", "llu.ios")

	response, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	parsedBody := LibreLinkGraphResponse{}
	responseBody, _ := ioutil.ReadAll(response.Body)

	if responseBody[0] != '{' && responseBody[0] != '[' {
		log.WithField("responseBody", string(responseBody)).Error("Got non-json response from LibreLinkUp")
		return []GraphData{}, errors.New("did not receive JSON from LibreLinkUp")
	}

	err = json.Unmarshal(responseBody, &parsedBody)
	if err != nil {
		log.Fatal(err)
	}

	return parsedBody.Data.GraphData, nil
}
