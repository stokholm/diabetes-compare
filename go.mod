module stokholm.industries/diabetes-compare

go 1.17

require (
	github.com/imroc/req v0.3.2
	github.com/sirupsen/logrus v1.8.1
	gorm.io/driver/postgres v1.3.8
	gorm.io/gorm v1.23.8
)
