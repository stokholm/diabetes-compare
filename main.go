package main

import (
	"fmt"
	"os"
	"time"

	log "github.com/sirupsen/logrus"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	gormLogger "gorm.io/gorm/logger"
)

// connectionIDS is a list of the connections you want to collect data for. The ID is a UUID style string you obtain using a MITM proxy like Charles.
var connectionIDS = []string{"00000000-0000-0000-0000-000000000000"}

func main() {

	// TODO: Ensure we have environment vars

	// Connect to db
	dsn := fmt.Sprintf("host=%s user=postgres password=%s dbname=libre-compare port=5432 sslmode=disable TimeZone=Europe/Stockholm", os.Getenv("DB_HOST"), os.Getenv("DB_PASSWORD"))
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: gormLogger.Default.LogMode(gormLogger.Info), // Enable this for query debugging
	})
	if err != nil {
		log.WithField("error", err).Fatal("Failed opening connection to database")
	}

	ensureAuth()
	collectGraphs(connectionIDS, db)
	ticker := time.NewTicker(1 * time.Hour)
	done := make(chan bool)

	go func() {
		for {
			select {
			case <-done:
				return
			case t := <-ticker.C:
				log.WithField("tick", t).Info("Collecting data")
				collectGraphs(connectionIDS, db)
			}
		}
	}()

	<-done
}

func collectGraphs(connectionIDs []string, db *gorm.DB) {

	for _, connectionID := range connectionIDs {
		graph, err := getGraphForConnection(connectionID)
		if err != nil {
			log.WithField("error", err).Error("Failed to parse body")
			return
		}

		log.WithField("connectionID", connectionID).Info("Got graph for connection")
		handleGraphData(connectionID, graph, db)
	}
}
