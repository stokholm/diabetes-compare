# Diabetes Compare

This small program will download the glucose graph of the Connection IDs you have set up at the top of `main.go`, once per hour, and save the data in a postgres database.

## Configuration

The program is configured using environment variables:

Variable | Used for
---------|-----
DB_HOST | the host of the postgres database
DB_PASSWORD | the password of the user in the database
LIBRE_USERNAME | the email address/username of the LibreLinkUp user that follows the connections you want to track
LIBRE_PASSWORD | the password of the user

The username, name for the database and it's port is currently hard coded, and is as follows:

- DB Username: posgres
- DB Name: libre-compare
- DB Port: 5432

Use the `db-structure.sql` file to create the table necessary for storing the data.
