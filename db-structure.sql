-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS graph_entries_id_seq;

-- Table Definition
CREATE TABLE "public"."graph_entries" (
    "id" int4 NOT NULL DEFAULT nextval('graph_entries_id_seq'::regclass),
    "connection" varchar(100),
    "value" float8,
    "measured_at" timestamp,
    "created_at" timestamp
);