FROM golang:1.17.3-alpine AS builder

WORKDIR /app
COPY . .

ENV USER=appuser
ENV UID=10001

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

RUN apk update && apk add --no-cache git ca-certificates
RUN go generate
RUN CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build -ldflags="-w -s" -o diabetes-compare

FROM scratch

# Copy user and group files
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

# Copy certs
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt

# Copy application
COPY --from=builder /app/diabetes-compare /app/diabetes-compare

USER appuser:appuser
ENTRYPOINT ["/app/diabetes-compare"]
